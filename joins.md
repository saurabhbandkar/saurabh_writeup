# JOINS in Database Queries

Generally, for a smaller dataset, all the data may be stored in a single database table. However, in all practical applications, data is stored in multiple database tables to save the operation time on processing larger datasets and to make the tables independent of each other.
Yet, the data needs to be maintained in a standard format to keep a relational link between each other and that's what is called data normalization.
For working with **JOINS**, the most important thing needed in different database tables is the need for a common column. A column that holds the same information is a must.
&nbsp;  

> Basic syntax of a JOIN Query:  
> &nbsp;
> SELECT column, anotherColumn, ... FROM table  
  &nbsp; &nbsp; &nbsp; &nbsp;INNER / LEFT OUTER / RIGHT OUTER / FULL OUTER / CROSS JOIN anotherTable  
  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;ON table.commonColumn = anotherTable.commonColumn;
&nbsp;  

Let's try to understand each one of the JOINS for the data set below:
|Table_1|Table_2|
|:--:|:--:|
|<table> <tr><th>nameId</th><th>names</th></tr><tr><th>1</th><th>Sachin</th></tr><tr><th>2</th><th>Virender</th></tr><tr><th>3</th><th>Sourav</th></tr><tr><th></th><th>Yuvraj</th></tr><tr><th>5</th><th>Zaheer</th></tr> <tr><th>6</th><th>Anil</th></tr> </table>| <table> <tr><th>surnameId</th><th>surname</th></tr><tr><th>5</th><th>Khan</th></tr><tr><th></th><th>Tendulkar</th></tr><tr><th>7</th><th>Srinath</th></tr> <tr><th>2</th><th>Sehwag</th></tr><tr><th>4</th><th>Singh</th></tr><tr><th>3</th><th>Ganguly</th></tr> </table>|
&nbsp;


## 1. INNER JOIN  
- **INNER JOIN** is used to obtain the *INTERSECTION* of available tables.
- **INNER JOIN** does not provide *NULL* values from the source tables.
- Query Syntax for INNER JOIN is:   
> SELECT * FROM Table_1 INNER JOIN Table_2 ON Table_1.nameId = Table_2.surnameId;

Which produces the following output-
<table> <tr><th>nameId</th><th>names</th><th>surnameId</th><th>surname</th></tr><tr><th>2</th><th>Virender</th><th>2</th><th>Sehwag</th></tr><tr><th>3</th><th>Sourav</th><th>3</th><th>Ganguly</th></tr><tr><th>5</th><th>Zaheer</th><th>5</th><th>Khan</th></tr>  </table>

&nbsp;  

## 2. LEFT OUTER JOIN  
- **LEFT OUTER JOIN** is used to obtain the *UNION* of available tables.
- **LEFT OUTER JOIN** provides all the rows from the left source table (table specified in FROM clause).
- **LEFT OUTER JOIN** provides *NULL* values from the right source table (table specified in LEFT OUTER JOIN clause) if a corresponding value from the left source table is not found. 
- Query Syntax for LEFT OUTER JOIN is:   
> SELECT * FROM Table_1 LEFT OUTER JOIN Table_2 ON Table_1.nameId = Table_2.surnameId;

Which produces the following output-
<table> <tr><th>nameId</th><th>names</th><th>surnameId</th><th>surname</th></tr><tr><th>1</th><th>Sachin</th><th>NULL</th><th>NULL</th></tr><tr><th>2</th><th>Virender</th><th>2</th><th>Sehwag</th></tr><tr><th>3</th><th>Sourav</th><th>3</th><th>Ganguly</th></tr><tr><th>5</th><th>Zaheer</th><th>5</th><th>Khan</th></tr> <tr><th>6</th><th>Anil</th><th>NULL</th><th>NULL</th></tr> </table>

&nbsp;  

## 3. RIGHT OUTER JOIN  
- **RIGHT OUTER JOIN** is used to obtain the *UNION* of available tables.
- **RIGHT OUTER JOIN** provides all the rows from the right source table (table specified in RIGHT OUTER JOIN clause).
- **RIGHT OUTER JOIN** provides *NULL* values from the left source table (table specified in FROM clause) if a corresponding value from the right source table is not found.
- Query Syntax for RIGHT OUTER JOIN is:   
> SELECT * FROM Table_1 RIGHT OUTER JOIN Table_2 ON Table_1.nameId = Table_2.surnameId;

Which produces the following output-
 <table> <tr><th>nameId</th><th>names</th><th>surnameId</th><th>surname</th></tr><tr><th>5</th><th>Zaheer</th><th>5</th><th>Khan</th></tr><tr><th>NULL</th><th>NULL</th><th>7</th><th>Srinath</th></tr> <tr><th>2</th><th>Virender</th><th>2</th><th>Sehwag</th></tr><tr><th>NULL</th><th>NULL</th><th>4</th><th>Singh</th></tr><tr><th>3</th><th>Sourav</th><th>3</th><th>Ganguly</th></tr> </table>

&nbsp;  

## 4. FULL OUTER JOIN  
- **FULL OUTER JOIN** is used to obtain the *UNION* of available tables.
- **FULL OUTER JOIN** provides all the rows from both the source tables.
- **FULL OUTER JOIN** provides *NULL* values from both the source tables if a corresponding value from both the source tables is not found.
- Query Syntax for RIGHT OUTER JOIN is:
> SELECT * FROM Table_1 FULL OUTER JOIN Table_2 ON Table_1.nameId = Table_2.surnameId;

Which produces the following output-
<table> <tr><th>nameId</th><th>names</th><th>surnameId</th><th>surname</th></tr><tr><th>1</th><th>Sachin</th><th>NULL</th><th>NULL</th></tr><tr><th>2</th><th>Virender</th><th>2</th><th>Sehwag</th></tr><tr><th>3</th><th>Sourav</th><th>3</th><th>Ganguly</th></tr><tr><th>5</th><th>Zaheer</th><th>5</th><th>Khan</th></tr><tr><th>6</th><th>Anil</th><th>NULL</th><th>NULL</th></tr><tr><th>NULL</th><th>NULL</th><th>7</th><th>Srinath</th></tr><tr><th>NULL</th><th>NULL</th><th>4</th><th>Singh</th></tr> </table>

&nbsp;  

## 4. CROSS JOIN  (Cartesian JOIN)
- **CROSS JOIN** is used to obtain the *UNION* of available tables.
- **CROSS JOIN** provides all the *combinations* of rows from both the source tables.
- **CROSS JOIN** does not provide *NULL* values from any tables.
- **CROSS JOIN** does not require a constraint column.
- Query Syntax for CROSS JOIN is:
> SELECT * FROM Table_1 CROSS JOIN Table_2;

Which produces the following output-
<table> <tr><th>nameId</th><th>names</th><th>surnameId</th><th>surname</th></tr>
<tr><th>1</th><th>Sachin</th><th>2</th><th>Sehwag</th></tr>
<tr><th>2</th><th>Virender</th><th>2</th><th>Sehwag</th></tr>
<tr><th>3</th><th>Sourav</th><th>2</th><th>Sehwag</th></tr>
<tr><th>5</th><th>Zaheer</th><th>2</th><th>Sehwag</th></tr>
<tr><th>6</th><th>Anil</th><th>2</th><th>Sehwag</th></tr>

<tr><th>1</th><th>Sachin</th><th>3</th><th>Ganguly</th></tr>
<tr><th>2</th><th>Virender</th><th>3</th><th>Ganguly</th></tr>
<tr><th>3</th><th>Sourav</th><th>3</th><th>Ganguly</th></tr>
<tr><th>5</th><th>Zaheer</th><th>3</th><th>Ganguly</th></tr>
<tr><th>6</th><th>Anil</th><th>3</th><th>Ganguly</th></tr>

<tr><th>1</th><th>Sachin</th><th>5</th><th>Khan</th></tr>
<tr><th>2</th><th>Virender</th><th>5</th><th>Khan</th></tr>
<tr><th>3</th><th>Sourav</th><th>5</th><th>Khan</th></tr>
<tr><th>5</th><th>Zaheer</th><th>5</th><th>Khan</th></tr>
<tr><th>6</th><th>Anil</th><th>5</th><th>Khan</th></tr>

<tr><th>1</th><th>Sachin</th><th>7</th><th>Srinath</th></tr>
<tr><th>2</th><th>Virender</th><th>7</th><th>Srinath</th></tr>
<tr><th>3</th><th>Sourav</th><th>7</th><th>Srinath</th></tr>
<tr><th>5</th><th>Zaheer</th><th>7</th><th>Srinath</th></tr>
<tr><th>6</th><th>Anil</th><th>7</th><th>Srinath</th></tr>


<tr><th>1</th><th>Sachin</th><th>4</th><th>Singh</th></tr>
<tr><th>2</th><th>Virender</th><th>4</th><th>Singh</th></tr>
<tr><th>3</th><th>Sourav</th><th>4</th><th>Singh</th></tr>
<tr><th>5</th><th>Zaheer</th><th>4</th><th>Singh</th></tr>
<tr><th>6</th><th>Anil</th><th>4</th><th>Singh</th></tr>

</table>

&nbsp;  

### Conclusion:  
Thus, **JOIN** queries are important to connect multiple database tables and retrieve normalized data.