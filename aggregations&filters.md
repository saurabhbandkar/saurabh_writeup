# AGGREGATIONS in Database Queries

Aggregation (AKA functions) is one of those capabilities that make you appreciate the power of relational database systems. It allows you to move beyond merely persisting your data, into the realm of asking truly interesting questions that can be used to inform decision making.
Basic syntax for use of Aggregation is:  
> SELECT AGG_FUNC(column_or_expression) AS aggregate_description, …  
> &nbsp; &nbsp; &nbsp; &nbsp; FROM mytable  
> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;WHERE constraint_expression;  

Let's see how we can work around with aggregations on the following dataset.

Table: Players
|nameid|name|attribute|runs|wickets|
|:-:|:-:|:-:|:-:|:-:|
|1|Sachin|Batsmen|10000|400|
|2|Sehwag|Batsmen|7500|50|
|3|Sourav|Batsmen|8000|150|
|4|VVS|Batsmen|6000|5|
|5|Rahul|Batsmen|8800|2|
|6|Yuvraj|Allrounder|5500|198|
|7|Harbhajan|Bowler|1000|500|
|8|Zaheer|Bowler|600|750|
|9|Kumble|Bowler|400|900|
|10|Agarkar|Bowler|800|650|
|11|Ashish|Bowler|200|575|

Following are some of the examples of aggregations:
1.  COUNT- Counts all the rows in the table/group with non-NULL values in the specified column.  
Syntax - SELECT COUNT(*) from Players;  

| COUNT(*) |
|:-:|
|11|

2.  SUM- Returns the sum of the selected column in the table/group.  
Syntax - SELECT SUM(wickets) from Players;  

| SUM(wickets) |
|:-:|
|4176|

3.  MAX- Returns the maximum value of the selected column in the table/group.  
Syntax - SELECT MAX(runs) from Players;  

| MAX(runs) |
|:-:|
|10000|

4.  MIN- Returns the minimum value of the selected column in the table/group.  
Syntax - SELECT MIN(runs) from Players;  

| MIN(runs) |
|:-:|
|200|

5.  AVG- Returns the average value of the selected column in the table/group.  
Syntax - SELECT AVG(wickets) from Players;  

| AVG(wickets) |
|:-:|
|380|


In addition to aggregating across all the rows, you can instead apply the aggregate functions to individual groups of data within that group (ie. runs for batsmen vs bowlers). The GROUP BY clause works by grouping rows that have the same value in the column specified.
Without a specified grouping, each aggregate function is going to run on the whole set of result rows and return a single value. And like normal expressions, giving your aggregate functions an alias ensures that the results will be easier to read and process.  
SQL has the GROUP BY construct. What this does is batch the data together into groups, and run the aggregation function separately for each group. When you specify a GROUP BY, the database produces an aggregated value for each distinct value in the supplied columns.
This would then create as many results as there are unique groups defined as by the GROUP BY clause.  
Basic syntax for use of GROUP BY keyword is:
>SELECT AGG_FUNC(column_or_expression) AS aggregate_description, …  
&nbsp; &nbsp; &nbsp; &nbsp;FROM mytable  
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;WHERE constraint_expression  
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;GROUP BY column;  


Following are some of the examples of aggregations with GROUP BY:
1.  COUNT- Counts all the rows in the group with non-NULL values in the specified column.  
Syntax - SELECT attribute, COUNT(nameid) from Players GROUP BY attribute;  

| attribute | COUNT(nameid) |
|:-:|:-:|
|Batsmen|5|
|Allrounder|1|
|Bowler|5|

2.  SUM- Returns the sum of the selected column in the table/group.  
Syntax - SELECT attribute, SUM(wickets) from Players GROUP BY attribute;  

| attribute | SUM(wickets) |
|:-:|:-:|
|Batsmen|607|
|Allrounder|198|
|Bowler|3375|

It turns out that there's an SQL keyword designed to help with the filtering of output from aggregate functions. This keyword is HAVING.

The behavior of HAVING is easily confused with that of WHERE. The best way to think about it is that in the context of a query with an aggregate function, WHERE is used to filter what data gets input into the aggregate function while HAVING is used to filter the data once it is output from the function.        
Basic syntax for use of HAVING keyword is:
>SELECT AGG_FUNC(column_or_expression) AS aggregate_description, …  
&nbsp; &nbsp; &nbsp; &nbsp;FROM mytable  
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;WHERE constraint_expression  
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;GROUP BY column;  
&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;HAVING constraint;  

Following are some of the examples of aggregations with GROUP BY and HAVING:
1.  MAX- Returns the maximum value of the selected column in the group.  
Syntax - SELECT attribute, MAX(runs) from Players GROUP BY attribute HAVING SUM(wickets) > 200;  

| attribute | MAX(runs) |
|:-:|:-:|
|Batsmen|10000|
|Bowler|1000|

2.  MIN- Returns the minimum value of the selected column in the group.  
Syntax - SELECT MIN(runs) from Players GROUP BY attribute HAVING SUM(wickets) < 1000;  

| attribute | MIN(runs) |
|:-:|:-:|
|Batsmen|6000|
|Allrounder|5500|


# FILTERS in Database Queries  
If we had a table with a hundred million rows of data, reading through all the rows would be inefficient and perhaps even impossible.
To filter certain results from being returned, we need to use a WHERE clause in the query. The clause is applied to each row of data by checking specific column values to determine whether they should be included in the results or not.  


Basic syntax for use of WHERE keyword:  
>SELECT column, another_column, …  
&nbsp; &nbsp; &nbsp; &nbsp;FROM mytable  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;WHERE condition  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;AND/OR another_condition  
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;AND/OR …;  


More complex clauses can be constructed by joining numerous AND or OR logical keywords (ie. num_wheels >= 4 AND doors <= 2). And below are some useful operators that you can use for numerical data (ie. integer or floating-point):  
| Operator | Condition |
|:-:|:-|
|=, !=, < <=, >, >= |Standard numerical operators|
|BETWEEN … AND …    |Number is within range of two values (inclusive)|
|NOT BETWEEN … AND …|   Number is not within range of two values (inclusive)|
|IN (…) |Number exists in a list|
|NOT IN (…)|Number does not exist in a list|

When writing WHERE clauses with columns containing text data, SQL supports several useful operators to do things like case-insensitive string comparison and wildcard pattern matching.

|Operator|  Condition|
|:-:|:-|
|=| Case sensitive exact string comparison (notice the single equals)|
|!= or <>|  Case sensitive exact string inequality comparison|
|LIKE   |Case insensitive exact string comparison|
|NOT LIKE|  Case insensitive exact string inequality comparison|
|%  |Used anywhere in a string to match a sequence of zero or more characters (only with LIKE or NOT LIKE)|
|_| Used anywhere in a string to match a single character|
|IN (…)|    String exists in a list|
|NOT IN (…)|    String does not exist in a list|

Following are some examples of WHERE keyword:
1. SELECT name FROM Players WHERE runs > 8500;  
Gives output as:  

| name |
| :--: |
| Sachin |
| Rahul |


2. SELECT name FROM Players WHERE runs >= 1000 AND wickets >= 150;
Gives output as:  
  

| name |
|:-:|
|Sachin|
|Sourav|
|Yuvraj|
|Harbhajan|

3. SELECT name FROM Players WHERE attribute LIKE '%Allrounder%;
Gives output as:  
  

| name |
|:-:|
|Yuvraj|

4. SELECT name FROM Players WHERE wickets BETWEEN 700 AND 1000;
Gives output as:  
  

| name |
|:-:|
|Zaheer|
|Kumble|

>Thus, it can be inferred that a filter can be used to obtain only the relevant data (rows) required from a large dataset.  
>Also, Aggregation along with grouping and filters for groups can be used to modify the available data and operate on only the relevant data. 

| Sr. no. | Resource |
| :------: | ------ |
| 1 |[https://sqlbolt.com/lesson/](https://sqlbolt.com/lesson/) |