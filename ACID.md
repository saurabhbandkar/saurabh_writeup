# Concept of ACID in Databases

DBMS is the management of data that should remain integrated when any changes are done in it. It is because if the integrity of the data is affected, whole data will get disturbed and corrupted. Therefore, to maintain the integrity of the data, there are four properties described in the database management system, which are known as the ACID properties.
The ACID properties enable you to write software without considering the complex environment in which the application runs.
The ACID properties are meant for the transaction that goes through a different group of tasks, and there we come to see the role of the ACID properties.

![N|Solid](https://static.javatpoint.com/dbms/images/acid-properties-in-dbms.png)

> ACID (atomicity, consistency, isolation, durability) is a set of properties of database transactions intended to guarantee data validity despite errors,  power failures, and other mishaps. In the context of databases, a sequence of database operations that satisfies the ACID properties (which can be perceived as a single logical operation on the data) is called a transaction. For example, a transfer of funds from one bank account to another, even involving multiple changes such as debiting one account and crediting another, is a single transaction.

## 1. Atomicity  
Atomicity of a database may be simplified as complete execution of a transaction or none at all.
Atomic databases either run a transaction successfully or terminate the entire operation but never executes a transaction partially.
Transactions are often composed of multiple statements. Atomicity guarantees that each transaction is treated as a single "unit", which either succeeds completely or fails: if any of the statements constituting a transaction fails to complete, the entire transaction fails and the database is left unchanged. An atomic system must guarantee atomicity in every situation, including power failures, errors, and crashes. A guarantee of atomicity prevents updates to the database from occurring only partially, which can cause greater problems than rejecting the whole series outright. As a consequence, the transaction cannot be observed to be in progress by another database client. At one moment in time, it has not yet happened, and at the next, it has already occurred in whole (or nothing happened if the transaction was canceled in progress).
If the application fails midway, the system will recover to the previous state, as if the transaction never took place.  

E.g., If we are booking movie tickets, we would select our seats and proceed to payment. But if the payment fails, the selected seats are freed up again as if they were not even selected. This is atomicity.

&nbsp;  

## 2. Consistency  
Consistency means that the data stored must be preserved in all states (before and after any transaction).
It ensures that a consistent database is available for all the transactions in the sequence that they execute.  

E.g., In the above case when we are selecting the seats, other users shall see that those seats are unavailable till the payment fails or succeeds.
If the payment fails, other users should be able to book those seats or if the payment succeeds, the seats shall stay booked and be unavailable.

&nbsp;  

## 3. Isolation  
The isolation property of a database ensures that each transaction is independent of the other. It ensures that one transaction doesn't affect the other.  
The individual actions of a transaction are scheduled to execute according to some notion of correctness. A schedule is serializable if it has the same effect as running the transactions serially, that is just the way you wrote the code. It is up to the system to produce a correct schedule.  

E.g., Isolation between transactions from various users simultaneously to book tickets is handled concurrently by the database and we do not need to worry about it in our code.

&nbsp;  

## 4. Durability  
Durability means that the data is stored permanently in the database after the successful operation of a transaction.
A durable system shall be able to recover data even in case of system failure or a crash.
This usually means that completed transactions (or their effects) are recorded in non-volatile memory.
With durability, you can concentrate on the application logic and not on failure detection, recovery, and synchronizing access to shared data. 

E.g., With our movie seat booking process, when the tickets are booked successfully, the booked seats are stored in the database.


&nbsp;  

Therefore, the ACID properties of DBMS play a vital role in maintaining the consistency and availability of data in the database.

&nbsp;  

### References


| Sr. no. | Resource |
| :------: | ------ |
| 1 |[https://www.bmc.com/blogs/acid-atomic-consistent-isolated-durable](https://www.bmc.com/blogs/acid-atomic-consistent-isolated-durable/) |
| 2 |[https://www.javatpoint.com/acid-properties-in-dbms](https://www.javatpoint.com/acid-properties-in-dbms) |
| 3 |[https://en.wikipedia.org/wiki/ACID](https://en.wikipedia.org/wiki/ACID) |